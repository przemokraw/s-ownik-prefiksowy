#ifndef PARSE_H
#define PARSE_H

#include <stdbool.h>

typedef struct Bufor
{
  char text[100001];
  char *cursor; //wskaźnik na bieżący znak
} Bufor;

enum Request
{
  REQUEST_INSERT,
  REQUEST_DELETE,
  REQUEST_CLEAR,
  REQUEST_FIND,
  REQUEST_PREV,
  REQUEST_ERROR,
  REQUEST_SUCCESS,
  REQUEST_QUIT
};

enum Request readLine(Bufor * l);
bool getNextArg(char **begin, char **end, Bufor *l);
enum Request getRequest(Bufor * l);
bool correctNumber(char **begin, char **end, Bufor * l);
int argToInt(char *begin);
bool correctWord(char ** begin, char ** end, Bufor * l);


#endif
