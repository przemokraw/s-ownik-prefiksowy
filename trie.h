#include <stdbool.h>

#include "list.h"

#define ALPH_LEN 26

/* *************** */
/* struktura węzła */
/* *************** */

typedef struct Node
{
  Word *w;  //wskaźnik na całe słowo
  char *begin; 
  char *end;  //wskaźniki na początek i koniec fragmentu, z którego węzeł korzysta
  char *wordBegin; //wskaźnik na początek słowa, którego podsłowem jest węzeł
  /* np: mamy w pamięci słowo "prokuratura" o id=0 i wywołujemy prev 0 3 6
   * czyli chcemy wstawić słow "kura", ale w słowniku jest już słowo "ku".
   * Zatem ostatecznie dodamy węzeł "ra" jako syna węzła "ku", czyli
   * begin będzie wskazywało na r, a end na a. Ale wordBegin będzie wskazywało
   * na k, żeby znać w każdym węźle całe słowo.
   * */
  int id; //numer >=0, jeśli węzeł jest słowem, -1 w p.p.
  struct Node * children[ALPH_LEN];
  struct Node * parent;
} Node;

typedef Node *Tree;

/* ********************* */
/* pomocnicze drzewo bst */
/* ********************* */

typedef struct BSTnode
{
  Tree key;
  struct BSTnode *left, *right;
} BSTnode;

typedef BSTnode *BSTtree;

void initBST(BSTtree *tree);
void addBST(BSTtree *tree, Node * node);
BSTnode * minValueNode(BSTtree tree);
BSTnode * searchBST(BSTtree tree, int id);
BSTnode * removeBST(BSTtree tree, BSTnode * toRemove);
void clearBST(BSTtree *tree);

/* ****************** */
/* funkcje pomocnicze */
/* ****************** */

int numberOfChildren(Node *node);
void resetChildren(Node *node);
int index(char c); //zwraca index w tablicy dla danego znaku
void printNodeLabel(Node *n);
void printTree(Tree tree);

/* ************** */
/* funkcje główne */
/* ************** */

void init(Tree *tree);
Node * pattern(Tree tree, char *w); //wyszukiwanie prefiksu
int addNode(Tree parent, Word * w, char * wordBegin, char * begin, char * end);
int splitNode(Tree node, char * begin, bool newWord); //podział węzła
int insert(Tree *tree, Word * w, char * wordBegin, char * begin, char * end);
Node* findByID(Tree tree, int ID);
int prev(Tree tree, int ID, int begin, int end);
void clearTree(Tree *tree);
void clearAll(Tree *tree);
int delete(Tree tree, int ID);

/* **************** */
/* zmienne globalne */
/* **************** */

int wordID; //id kolejnych słów
WordList list; //lista zaalokowanych w pamięci słów
int numOfNodes; //liczba wszystkich węzłów
BSTtree bst; //drzewo bst do przechowywania wskaźników na istniejące w słowniku słowa
