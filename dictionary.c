#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <getopt.h>


#include "list.h"
#include "parse.h"
#include "trie.h"

bool printNodeNum = false;

int main(int argc, char **argv)
{
  //obsługa parametrów wywołania
  int c;
  while((c = getopt(argc, argv, "v")) != -1)
  {
    switch(c)
    {
      case 'v':
        printNodeNum = true;
        break;
      case '?':
        fprintf(stderr, "Nieznana opcja: `\\x%x'.\n", optopt);
        return 1;
    }
  }

  Tree tree;
  init(&tree);
  Bufor b;
  Bufor *buf;
  buf = &b;
  int num;
  enum Request end;
  enum Request req;
  while(end = readLine(buf))
  {
    num = -1; 
    req = getRequest(buf);
    if((req == REQUEST_ERROR) && (end != REQUEST_QUIT))
    {
      printf("ignored\n");
    }
    /*---------- CLEAR ----------*/
    else if(req == REQUEST_CLEAR)
    {
      clearAll(&tree);
      printf("cleared\n");
      num = 1;
    }
    /*--------- DELETE ----------*/
    else if(req == REQUEST_DELETE)
    {
      char *begin, *end;
      if(correctNumber(&begin, &end, buf))
      {
        int toDel = argToInt(begin);
        //czy są inne argumenty?
        if(!getNextArg(&begin, &end, buf))
        {
          num = delete(tree, toDel);
          if(num == -1)
            printf("ignored\n");
          else
            printf("deleted: %d\n", num);
        }
        else
        {
          printf("ignored\n");
        }
      }
      else
      {
        printf("ignored\n");
      }

    }
    /*---------- INSERT ----------*/
    else if(req == REQUEST_INSERT)
    {
      char *begin, *end;
      if(correctWord(&begin, &end, buf))
      {
        char * newWord = malloc(sizeof(char)*(end-begin+2));
        int  i = 0;
        while(begin <= end)
        {
          newWord[i] = *begin;
          begin++;
          i++;
        }
        newWord[i] = '\0';

        //czy są inne argumenty?
        if(!getNextArg(&begin,&end,buf))
        {
          Word * word = addToList(list,newWord);
          newWord = NULL;
          num = insert(&tree,word,word->text,word->text,getLastChar(word));
          if(word->linkedNodes == 0)
            removeWord(word);

          if(num == -1)
            printf("ignored\n");
          else
            printf("word number: %d\n", num);
        }
        else
        {
          num = -1; // trochę sztucznie, na potrzeby -v
          printf("ignored\n");
        }
      }
      else
      {
        num = -1;
        printf("ignored\n");
      }
    }
    /*---------- PREV -----------*/
    else if(req == REQUEST_PREV)
    {
      char *begin, *end;
      int args[3];
      int i = 0;
      bool ok = true;
      

      while((i<3) && ok)
      {
        if(correctNumber(&begin, &end, buf))
          args[i] = argToInt(begin);
        else
          ok = false;
        i++;
      }

      //czy są inne argumenty?
      if(!getNextArg(&begin, &end, buf))
      {
        if(!ok)
          printf("ignored\n");
        else
        {
          num = prev(tree,args[0],args[1],args[2]);
          if(num == -1)
            printf("ignored\n");
          else
            printf("word number: %d\n", num);
        }
      }
      else
      {
        printf("ignored\n");
      }
    }
    /*---------- FIND -----------*/
    else if(req == REQUEST_FIND)
    {
      char *begin, *end;
      if(correctWord(&begin, &end, buf))
      {
        char * newWord = malloc(sizeof(char)*(end-begin+2));
        int  i = 0;
        while(begin <= end)
        {
          newWord[i] = *begin;
          begin++;
          i++;
        }
        newWord[i] = '\0';

        //czy są inne argumenty?
        if(!getNextArg(&begin, &end, buf))
        {
          if(pattern(tree, newWord))
            printf("YES\n");
          else
            printf("NO\n");
        }
        else
        {
          printf("ignored\n");
        }

        free(newWord);
      }
      else
      {
        printf("ignored\n");
      }
    }

    if(end == REQUEST_QUIT)
    {
      clearAll(&tree);
      clearList(&list);
      break;
    }


    if((req != REQUEST_FIND) && (req != REQUEST_ERROR) && (num > -1) && printNodeNum)
    {
      if(numOfNodes == 1)
        fprintf(stderr, "nodes: %d\n", 0);
      else
        fprintf(stderr, "nodes: %d\n", numOfNodes);
    }


  }



  return 0;
}
