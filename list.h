/* lista dwukierunkowa z atrapamido przechowywania słów zaalokowanych
 * w pamięci. Nie będzie potrzeby iterowania po tej liście. 
 */

#ifndef LIST_H
#define LIST_H

typedef struct Word
{
  char *text; //treść słowa
  int linkedNodes; //ile węzłów korzysta z tego słowa?
  struct Word *prev;
  struct Word *next;
} Word;

typedef Word *WordList;

WordList newList(); //tworzy nową pustą listę
Word* addToList(WordList list, char *w); //dodaje na początek listy nowe słówo i zwraca wskaźnik do niego
void removeWord(Word *w);
void clearList(WordList *list);
char* getLastChar(Word *w);
void printList(WordList list); //do debugowania

#endif
