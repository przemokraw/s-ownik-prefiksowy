#include <stdlib.h> //malloc, free
#include <string.h>
#include <stdbool.h> //pattern
#include <stdio.h> ////printf

#include "trie.h"
#include "list.h"

/* *********************************** */
/* funkcje dla pomocniczego drzewa bst */
/* *********************************** */

void initBST(BSTtree *tree)
{
  *tree = NULL;
}

void addBST(BSTtree *tree, Node *node)
/* dodaję węzeł do drzewa */
{
  if(*tree == NULL)
  {
    *tree = malloc(sizeof(BSTnode));
    (*tree)->key = node;
    (*tree)->left = NULL;
    (*tree)->right = NULL;
  }
  else if((*tree)->key->id > node->id)
  {
    addBST(&((*tree)->left), node);
  }
  else if((*tree)->key->id < node->id)
  {
    addBST(&((*tree)->right), node);
  }
}

BSTnode * minValueNode(BSTtree tree)
{
  while(tree->left != NULL)
    tree = tree->left;

  return tree;
}

BSTnode* searchBST(BSTtree tree, int id)
{

  while(tree != NULL)
  {

    if(tree->key->id == id)
    {

      return tree;
    }

    if(tree->key->id > id)
      tree = tree->left;
    else
      tree = tree->right;
  }
  //printf("NULL\n");
  return NULL;
}


BSTnode * removeBST(BSTtree tree, BSTnode * toRemove)
{
  if(tree == NULL)
    return NULL;

  if(toRemove->key->id < tree->key->id)
    tree->left = removeBST(tree->left, toRemove);

  else if(toRemove->key->id > tree->key->id)
    tree->right = removeBST(tree->right, toRemove);

  else
  {
    if(tree->left == NULL)
    {
      BSTnode * dummy = tree->right;
      free(tree);
      return dummy;
    }
    else if(tree->right == NULL)
    {
      BSTnode * dummy = tree->left;
      free(tree);
      return dummy;
    }
    else
    {
      BSTnode * dummy = minValueNode(tree->right);
      tree->key = dummy->key;
      tree->right = removeBST(tree->right, dummy);
    }
  }
  return tree;
}

void clearBST(BSTtree *tree)
{
  if(*tree != NULL)
  {
    clearBST(&((*tree)->left));
    clearBST(&((*tree)->right));
    free(*tree);
    *tree = NULL;
  }
}


/* ****************** */
/* funkcje pomocnicze */
/* ****************** */

int numberOfChildren(Node *node)
{
  int numberOfChildren = 0;
  for(int i = 0; i < ALPH_LEN; i++)
    if(node->children[i] != NULL) numberOfChildren++;

  return numberOfChildren;
}

void resetChildren(Node *node)
{
  for(int i = 0; i < ALPH_LEN; i++)
    node->children[i] = NULL;
}

int index(char c)
{
  return c - 'a';
}

void printNodeLabel(Node *n)
{
  char *it = n->begin;
  while(it != (n->end)+1)
  {
    printf("%c", *it);
    it++;
  }
  printf("\n");
}

void printTree(Tree tree)
{
  if(tree == NULL)
    return;

  for(int i = 0; i < ALPH_LEN; i++)
    if(tree->children[i] != NULL)
      printTree(tree->children[i]);

  if(tree->id != -2)
  {
    printNodeLabel(tree);
  }
}

/* ************** */
/* funkcje główne */
/* ************** */

void init(Tree *tree)
{
  *tree = NULL;
  wordID = 0;
  list = newList();
  initBST(&bst);
  numOfNodes = 0;
}

Node* pattern(Tree tree, char *w)
/* szuka prefiksu,
 * zakładam, że w jest niepuste
 */
{
  if(tree == NULL)
    return NULL;

  if(tree->children[index(w[0])] == NULL)
    return NULL;

 
  tree = tree->children[index(w[0])]; //przechodzę do właściwej gałęzi...
  char *treeIter = tree->begin;
  char *wordIter = w;

  //...i idę wgłąb:
  while(tree != NULL)
  {
      //skonczylo sie sprawdzane słowo
      if(*wordIter == '\0')
      {
        return tree;
      }
      //skończyło się słowo węźle
      else if(treeIter == (tree->end)+1)
      {
        //jest kolejny węzeł:
        if(tree->children[index(*wordIter)] != NULL)
        {
          tree = tree->children[index(*wordIter)];
          treeIter = tree->begin;
        }
        //ne ma kolejnego węzła:
        else
        {
          return NULL;
        }
      }
      //nic się nie skończyło:
      else if(*wordIter != *treeIter)
      {
        return NULL;
      }
      else
      {
        wordIter++;
        treeIter++;
      }
  }
}

int addNode(Tree parent, Word * w, char * wordBegin, char * begin, char * end)
/* dodaję syna ze słowem od begin do end do węzła, 
 * który jeszcze nie ma żadnego syna
 * na literę *begin
 * */
{
  Node * new = malloc(sizeof(Node));
  new->w = w;
  (new->w->linkedNodes)++; //zwiększam liczbę powiązanych węzłów danego Word-a
  new->wordBegin = wordBegin;
  new->begin = begin;
  new->end = end;
  new->id = wordID; wordID++;
  new->parent = parent;
  addBST(&bst, new);
  resetChildren(new);

  //zwiększam całkowitą liczbę węzłów
  numOfNodes++;

  parent->children[index(*begin)] = new;

  return new->id;
}

int splitNode(Tree node, char * begin, bool newWord)
/* dzieli węzeł na dwa względem znaku begin.
 * newWord = true -> nowy węzeł ma być słowem
 * newWord = false -> nowy węzeł nie ma być słowem
 * */
{
  Node *new = malloc(sizeof(Node));
  new->w = node->w;
  (new->w->linkedNodes)++; //zwiększam liczbę powiązanych węzłów danego Word-a
  new->wordBegin = node->wordBegin;
  new->end = begin-1;
  new->begin = node->begin;
  if(newWord)
  {
    new->id = wordID; wordID++;
    addBST(&bst, new);
  }
  else
  {
    new->id = -1;
  }
  new->parent = node->parent;
  new->parent->children[index(*(new->begin))] = new;
  resetChildren(new);

  new->children[index(*begin)] = node;

  //teraz muszę się zająć starym węzłem
  node->begin = begin;
  node->parent = new;

  //zwiększam całkowitą liczbę węzłów
  numOfNodes++;

  return new->id;
}

int insert(Tree *tree, Word * w, char * wordBegin, char * begin, char * end)
/* wstawia do drzewa słowo zdefiniowane przez argumenty 2-5 */
{
  /* 1) drzewo puste */
  if(*tree == NULL)
  {
    *tree = malloc(sizeof(Node));
    (*tree)->id = -2; //id zarezerwowane dla korzenia
    (*tree)->w = NULL;
    (*tree)->begin = NULL;
    (*tree)->end = NULL;
    (*tree)->parent = NULL;
    (*tree)->wordBegin = NULL;
    resetChildren(*tree);
    numOfNodes++;

    return addNode(*tree, w, wordBegin, begin, end);
  }

  /* 2) drzewo niepuste, ale nie ma jeszcze słów na daną literę */
  if((*tree)->children[index(*begin)] == NULL)
  {
    return addNode(*tree, w, wordBegin, begin, end);
  }

  /* 3) są już słowa na daną literę */
  Node * tr = *tree;
  tr = tr->children[index(*wordBegin)];
  char *treeIter = tr->begin;
  char *wordIter = begin;

  while(tr != NULL)
  {
    /* skończyło się słowo do wstawiania */
    if(wordIter == end + 1)
    {
      /* podział węzła */
      if(treeIter <= tr->end)
      {
        return splitNode(tr,treeIter,true);
      }
      /* zmiana statusu węzła */
      else
      {
        if(tr->id != -1)
        {
          return -1;
        }
        else
        {
          tr->id = wordID; wordID++;
          addBST(&bst, tr);
          return tr->id;
        }
      }
    }
    /* skończyło się słowo węźle, ale to do wstawienia nie */
    else if(treeIter == (tr->end)+1)
    {
      /* w tr można zmienić Word */
      if(tr->w != w)
      {
        (tr->w->linkedNodes)--;
        if(tr->w->linkedNodes == 0)
          removeWord(tr->w);
        int trLabelLength = tr->end - tr->begin + 1;
        tr->w = w;
        (tr->w->linkedNodes)++;
        tr->wordBegin = wordBegin;
        tr->end = wordIter-1;
        tr->begin = tr->end - trLabelLength + 1;
      }
      /* da się iść do następnego węzła */
      if(tr->children[index(*wordIter)] != NULL)
      {
        tr = tr->children[index(*wordIter)];
        treeIter = tr->begin;
      }
      /* nie da się iść do kolejnego węzła */ 
      else
      {
        return addNode(tr, w, wordBegin, wordIter, end);
      }
    }
    /* nic się nie skończyło */
    else
    {
      /* znaki są równe */
      if(*wordIter == *treeIter)
      {
        wordIter++;
        treeIter++;
      }
      /* jest różnica i trzeba dzielić węzły. */
      else
      {
        int n = splitNode(tr, treeIter, false);
        return addNode(tr->parent, w, wordBegin, wordIter, end);
      }
    }
  }
}


void clearTree(Tree *tree)
{
  if(*tree == NULL)
    return;

  for(int i = 0; i < ALPH_LEN; i++)
    if((*tree)->children[i] != NULL)
      clearTree(&((*tree)->children[i]));

  free(*tree);
  tree = NULL;
}

void clearAll(Tree *tree)
{
  clearList(&list);
  clearTree(tree);
  clearBST(&bst);
  init(tree);
}

Node * findByID(Tree tree, int ID)
{
  BSTnode * found = searchBST(bst, ID);
  if(found == NULL)
    return NULL;
  else
    return found->key;
}

int prev(Tree tree, int ID, int begin, int end)
{
  Node *found = findByID(tree,ID);

  if(found == NULL)
    return -1;

  if(begin > end)
    return -1;
  if(found->end - found->wordBegin < end)
    return -1;

  return insert(&tree, found->w, found->wordBegin + begin, found->wordBegin + begin, found->wordBegin + end);
}

int delete(Tree tree, int ID)
{
  Node * toRemove = findByID(tree, ID);

  if(toRemove == NULL)
    return -1;

  BSTnode * dummy = searchBST(bst, toRemove->id);
  bst = removeBST(bst, dummy);

  if(numberOfChildren(toRemove) == 0)
  {
    Node * parent = toRemove->parent;
    parent->children[index(*(toRemove->begin))] = NULL;
    free(toRemove);
    numOfNodes--;

    if((numberOfChildren(parent) == 1) && (parent->id == -1))
    {
      int n;
      for(int i = 0; i < ALPH_LEN; i++)
        if(parent->children[i] != NULL) n = i;

      int parentLabelLength = parent->end - parent->begin + 1;
      Node * son = parent->children[n];
      son->begin = son->begin - parentLabelLength;
      son->parent = parent->parent;
      parent->parent->children[index(*(son->begin))] = son;
      free(parent);
      numOfNodes--;
    }
  }
  else if(numberOfChildren(toRemove) == 1)
  {
    int n;
    for(int i = 0; i < ALPH_LEN; i++)
      if(toRemove->children[i] != NULL) n = i;

    int toRemoveLabelLength = toRemove->end - toRemove->begin + 1;
    Node * son = toRemove->children[n];
    son->begin = son->begin - toRemoveLabelLength;
    son->parent = toRemove->parent;
    toRemove->parent->children[index(*(son->begin))] = son;
    free(toRemove);
    numOfNodes--;
  }
  else
  {
    toRemove->id = -1;
  }

  return ID;
}
