#include <stdlib.h> //malloc, free
#include <stdio.h> //printf
#include <string.h>

#include "list.h"

WordList newList()
{
  //atrapa na początku:
  WordList dummyBegin = malloc(sizeof(Word));
  dummyBegin->next = NULL;
  dummyBegin->prev = NULL;
  dummyBegin->linkedNodes = -1;
  dummyBegin->text = NULL;

  //atrapa na końcu:
  WordList dummyEnd = malloc(sizeof(Word));
  dummyEnd->next = NULL;
  dummyEnd->prev = NULL;
  dummyEnd->linkedNodes = -1;
  dummyEnd->text = NULL;

  dummyBegin->next = dummyEnd;

  return dummyBegin;
}

Word* addToList(WordList list, char *w)
/* tutaj w będzie słowem wczytanym z wejścia
 * i trzeba zaalokować dla niego miejsce
 * */
{
  Word * new = malloc(sizeof(Word));
  new->linkedNodes = 0;
  new->text = w;
  for(int i = 0; i < strlen(w); i++)
    new->text[i] = w[i];
  new->text[strlen(w)] = '\0';
  
  new->next = list->next;
  new->prev = list;
  list->next = new;
  new->next->prev = new;

  return new;
}

void removeWord(Word *w)
{
  w->next->prev = w->prev;
  w->prev->next = w->next;

  free(w->text); //jako argument na pewno nie podam atrap
  free(w);
}

void clearList(WordList *list)
{
  while((*list)->next->linkedNodes != -1)
    removeWord((*list)->next);

  free((*list)->next); //atrapa koncowa
  free((*list));
  *list = NULL;
}

char* getLastChar(Word *w)
{
  return &(w->text[strlen(w->text)-1]);
}

void printList(WordList list)
{
  Word * iter = list->next;
  while(iter->linkedNodes != -1)
  {
    printf("(%d): %s, ", iter->linkedNodes, iter->text);
    iter = iter->next;
  }
}
