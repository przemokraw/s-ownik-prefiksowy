files=dictionary.o trie.o parse.o list.o
opts=--std=c99
dbg=-g


dictionary: $(files)
	cc $(opts) -o dictionary $(files)

debug: $(files)
	cc $(dbg) $(opts) -o dictionary.dbg $(files)

parse.o: parse.c parse.h
	cc $(opts) -c parse.c

trie.o: trie.c trie.h list.h
	cc $(opts) -c trie.c

list.o: list.c list.h
	cc $(opts) -c list.c

dictionary.o: dictionary.c trie.h parse.h list.h
	cc $(opts) -c dictionary.c

clean:
	-rm $(files) dictionary
