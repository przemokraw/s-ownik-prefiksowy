#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

#include "parse.h"

enum Request readLine(Bufor *l)
/* wczytuje całą linię do bufora
 * */
{
  int c;
  int i = 0;
  while(((c=getc(stdin)) != '\n') && (c != EOF))
  {
    l->text[i] = c;
    i++;
  }

  l->text[i] = '\0';
  l->cursor = l->text;
  if(c != EOF)
  {
    //ungetc(c,stdin);
    return REQUEST_SUCCESS;
  }
  else
  {
    return REQUEST_QUIT;
  }
}

bool getNextArg(char **begin, char **end, Bufor * l)
/* ustawia wskaźniki begin i end na odpowiednio początek
 * i koniec kolejnego słowa w linii
 * Zwraca true, jeśli istnieje słowo, które można wczytać,
 * false w p.p. */
{
  while(*(l->cursor) == ' ') (l->cursor)++;

  if((*(l->cursor) == '\0') || (*(l->cursor) == EOF))
    return false;
  
  *begin = l->cursor;
  while((*(l->cursor) != EOF) && (*(l->cursor) != '\0') && (*(l->cursor) != ' '))
    (l->cursor)++;

  *end = l->cursor - 1;
  return true;
}

enum Request getRequest(Bufor * l)
{
  char *begin, *end;
  if(getNextArg(&begin, &end, l))
  {
    if(begin - end + 1 > 6) //najdłuższe poprawne polecenie ma 6 znaków
    {
      return REQUEST_ERROR;
    }
    else
    {
      char req[7];
      int i = 0;
      while(begin <= end)
      {
        req[i] = *begin;
        begin++;
        i++;
      }
      req[i] = '\0';

      if(strcmp(req,"clear") == 0)
        return REQUEST_CLEAR;
      else if(strcmp(req,"delete") == 0)
        return REQUEST_DELETE;
      else if(strcmp(req,"insert") == 0)
        return REQUEST_INSERT;
      else if(strcmp(req,"prev") == 0)
        return REQUEST_PREV;
      else if(strcmp(req,"find") == 0)
        return REQUEST_FIND;
      else
        return REQUEST_ERROR;
      
    }
  }
  else
  {
    if((*(l->cursor) == '\0'))
      return REQUEST_ERROR;
    else // l->cursor == EOF
      return REQUEST_QUIT;
  }
}

bool correctNumber(char **begin, char **end, Bufor *l)
/* pobiera kolejny argument i sprawdza, czy jest poprawną liczbą
 * ustawia begin i end onpowiednio na koniec i początek liczby*/
{ 
  //TODO sprawdzić zakres liczby!
  if(getNextArg(begin, end, l))
  {
    if((*begin != *end) && (**begin == *(*begin+1)) && (**begin == '0')) //dwa zera na początku
    {
      return false;
    }
    else
    {
      char maxint[20];
      char num[20];
      int i = 0;
      sprintf(maxint,"%d", INT_MAX);
      char *iter = *begin;
      while((iter <= *end) && (i < 19))
      {
        if((*iter < '0') || (*iter > '9'))
          return false;
        num[i] = *iter;
        i++;
        iter++;
      }
      if(i == 20)
        return false;

      num[i] = '\0';

      if(strlen(num) > strlen(maxint))
        return false;
      else if((strlen(num) == strlen(maxint)) && (strcmp(maxint,num) < 0))
        return false;

      return true;
    }
  }
  else //nie wczytano żadnego argumentu
  {
    return false;
  }
}

int argToInt(char *begin)
/* zamienia poprawny argument liczbowy na int */
{
  char *dummy;
  return strtol(begin, &dummy, 10);
}

bool correctWord(char **begin, char **end, Bufor *l)
{
  if(getNextArg(begin, end, l))
  {
    char *iter = *begin;
    while(iter <= *end)
    {
      if((*iter < 'a') || (*iter > 'z'))
        return false;
      iter++;
    }
    return true;
  }
  else
  {
    return false;
  }
}

